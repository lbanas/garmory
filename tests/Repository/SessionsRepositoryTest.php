<?php namespace App\Tests\Repository;

use App\Entity\Sessions;
use App\Repository\SessionsRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SessionsRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    protected function setUp() : void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function testGetHighestNumber() : void
    {
        $repo = $this->em->getRepository(Sessions::class);
        $repo->setCalculationMethod(SessionsRepository::CALCULATION_METHOD_MATRIX);
        $this->assertEquals('321', $repo->getHighestNumber(['1', '2', '3']));
        $this->assertEquals('20110', $repo->getHighestNumber(['10', '1', '20']));
        $this->assertEquals('110100', $repo->getHighestNumber(['100', '10', '1']));
        $this->assertEquals('7702550000400125', $repo->getHighestNumber(['50000', '25', '4001', '7', '702', '5']));
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown() : void
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }
}