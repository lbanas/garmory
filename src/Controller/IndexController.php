<?php

namespace App\Controller;

use App\Entity\Sessions;
use App\Exceptions\WrongNumberException;
use App\Repository\SessionsRepository;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @param null $params
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, $params = null)
    {
        $form = $this->getForm();
        $form->handleRequest($request);
        $previousTests = $this->getDoctrine()->getRepository(Sessions::class)->findAll();
        $status = '';
        $result = '';

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $numbers = $data['numbers'];
            $status = 'Successfully calculated data';

            /** @var SessionsRepository $session */
            $session = $this->getDoctrine()->getRepository(Sessions::class);
            $session->setCalculationMethod($this->container->getParameter('calculation_method'));
            $result = $session->getHighestNumber($numbers);

            try {
                if ($this->container->getParameter('save_sessions')) {
                    $session->saveSession($result);
                }

                unset($form);
                $form = $this->getForm();
            } catch (WrongNumberException $numberException) {
                $status = $numberException->getMessage();
            } catch (\Doctrine\ORM\ORMException $ORMException) {
                $status = 'Successfully calculated data, but error occurred during saving';
            }
        }

        return $this->render('index/index.html.twig', [
            'form' => $form->createView(),
            'message' => $status,
            'result' => $result,
            'previous_tests' => $previousTests
        ]);
    }

    protected function getForm() : FormInterface
    {
        return $this->createFormBuilder()
            ->add('numbers', CollectionType::class, [
                'entry_type' => IntegerType::class,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'entry_options' => [
                    'attr' => ['class' => 'form-control']
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Process',
                'attr' => [
                    'class' => 'btn btn-primary',
                    'disabled' => 'disabled'
                    ]
            ])
            ->getForm();
    }
}
