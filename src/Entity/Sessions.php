<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionsRepository")
 */
class Sessions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Number", mappedBy="sessions", orphanRemoval=true)
     */
    private $numbers;

    /**
     * @ORM\Column(type="text")
     */
    private $result;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {
        $this->numbers = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|Number[]
     */
    public function getNumbers(): Collection
    {
        return $this->numbers;
    }

    public function addNumber(Number $number): self
    {
        if (!$this->numbers->contains($number)) {
            $this->numbers[] = $number;
            $number->setSessions($this);
        }

        return $this;
    }

    public function removeNumber(Number $number): self
    {
        if ($this->numbers->contains($number)) {
            $this->numbers->removeElement($number);
            // set the owning side to null (unless already changed)
            if ($number->getSessions() === $this) {
                $number->setSessions(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt() : ?\DateTime
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     * @return Sessions
     */
    public function setCreatedAt(\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    /**
     * @param $result
     * @return Sessions
     */
    public function setResult($result): self
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @return string
     */
    public function getAllNumbersAsString() : string
    {
        $return = [];

        /** @var Number $single */
        foreach ($this->numbers as $single) {
            $return[] = $single;
        }

        return implode(', ', $return);
    }
}
