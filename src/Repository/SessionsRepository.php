<?php

namespace App\Repository;

use App\Entity\Number;
use App\Entity\Sessions;
use App\Exceptions\WrongNumberException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Sessions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sessions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sessions[]    findAll()
 * @method Sessions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionsRepository extends ServiceEntityRepository
{
    private $numbers = [];

    private $result = 0;

    private $maxLength;

    private $calculationMethod;

    private const WILDCARD = 10;

    public const CALCULATION_METHOD_COMBINATIONS = 'combinations';

    public const CALCULATION_METHOD_MATRIX = 'matrix';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sessions::class);

        $this->setCalculationMethod(self::CALCULATION_METHOD_COMBINATIONS);
    }

    /**
     * @param string $method
     */
    public function setCalculationMethod(string $method) : void
    {
        if (!\in_array($method, [self::CALCULATION_METHOD_COMBINATIONS, self::CALCULATION_METHOD_MATRIX], true)) {
            throw new \LogicException('unknown calculation method');
        }
        $this->calculationMethod = $method;
    }

    /**
     * @param mixed ...$args
     * @return string
     * @throws WrongNumberException
     */
    public function getHighestNumber(...$args) : string
    {
        if (empty($args)) {
            throw new WrongNumberException('None numbers set!');
        }

        $args = end($args);

        // clear results for unit test
        $this->numbers = [];
        $this->result = 0;
        $this->maxLength = null;

        foreach ($args as $single) {
            if ((int) $single === 0) {
                throw new WrongNumberException('Value ' . $single . ' is not a number!');
            }

            $number = new Number();
            $number->setValue($single);
            $this->numbers[] = $number;
        }

        return $this->getCompactedValue();
    }

    /**
     * @param $result
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveSession($result) : void
    {
        $session = new Sessions();
        $session->setResult($result);
        $now = new \DateTime('now');
        $session->setCreatedAt($now);

        $em = $this->getEntityManager();
        $em->persist($session);

        /** @var Number $number */
        foreach ($this->numbers as $number) {
            $number->setSessions($session);
            $em->persist($number);
        }

        $em->flush();
    }

    private function getCompactedValue() : string
    {
        if ($this->calculationMethod === self::CALCULATION_METHOD_MATRIX) {
            $this->findByMatrix($this->numbers);
        } else {
            $this->findAllPermutations($this->numbers);
        }

        return $this->result;
    }

    /**
     * @param $numbers
     * @param array $compact
     */
    private function findAllPermutations($numbers, array $compact = []) : void
    {
        if (empty($numbers)) {
            $result = (int) \implode('', $compact);

            if ($result > $this->result) {
                $this->result = $result;
            }
        } else {
            for ($i = \count($numbers) - 1; $i >= 0; --$i) {
                $newNumbers = $numbers;
                $compactedNumbers = $compact;
                [$foo] = array_splice($newNumbers, $i, 1);
                array_unshift($compactedNumbers, $foo);
                $this->findAllPermutations($newNumbers, $compactedNumbers);
            }
        }
    }

    /**
     * @param array $data
     */
    private function findByMatrix(array $data) : void
    {
        $this->getMaxLength($data);
        $matrix = $this->getMatrix($data, $this->maxLength);

        usort($matrix, [$this, 'sortMatrix']);
        $result = $this->cleanMatrix($matrix);
        $this->result = implode('', $result);
    }

    /**
     * @param array $array
     * @return int
     */
    private function getMaxLength(array $array) : void
    {
        $maxLength = 0;

        /** @var Number $entry */
        foreach($array as $entry) {
            $length = strlen($entry);

            if ($length > $maxLength) {
                $maxLength = $length;
            }
        }

        $this->maxLength = $maxLength - 1;
    }

    /**
     * @param array $array
     * @param $length
     * @return array
     */
    private function getMatrix(array $array, $length) : array
    {
        $matrix = [];

        foreach ($array as $number) {
            $row = [];
            $input = str_split($number);

            for ($x = 0; $x <= $length; $x++) {
                $row[$x] = array_key_exists($x, $input) ? $input[$x] : self::WILDCARD;
            }

            $matrix[] = $row;
        }

        return $matrix;
    }

    /**
     * @param $a
     * @param $b
     * @return int
     */
    private function sortMatrix($a, $b) : int
    {
        $result = 0;

        for ($x = 0; $x <= $this->maxLength; $x++) {
            if ((int) $a[$x] > (int) $b[$x]) {
                $result = -1;
                break;
            }

            if ((int) $a[$x] < (int) $b[$x]) {
                $result = 1;
                break;
            }
        }

        return $result;
    }

    /**
     * @param array $matrix
     * @return array
     */
    private function cleanMatrix(array $matrix) : array
    {
        $result = [];

        foreach ($matrix as $rowIndex => $row) {
            foreach ($row as $entryIndex => $entry) {
                if ((int) $entry === self::WILDCARD) {
                    unset($matrix[$rowIndex][$entryIndex]);
                }
            }
        }

        foreach ($matrix as $m) {
            $result[] = implode('', $m);
        }

        return $result;
    }
}
