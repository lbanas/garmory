var $ = require('jquery');
require('bootstrap-sass');

$(document).ready(function() {
    function PageController() {
        this.multiAddSelector = '.add-another-collection-widget';
        this.submitButtonSelector = '#form_save';
        this.removeButtonSelector = 'a.remove-li';

        this.init = function() {
            this.initPopover();
            this.initMultiAdd();
            this.unlockSubmitButton();
            this.bindRemoveButton();
        };

        this.initPopover = function () {
            $('[data-toggle="popover"]').popover();
        };

        this.initMultiAdd = function () {
            var self = this;

            $(this.multiAddSelector).click(function (e) {
                e.preventDefault();
                var list = $($(this).attr('data-list'));
                var counter = list.data('widget-counter') | list.children().length;

                if (!counter) { counter = list.children().length; }

                if (counter >= 10) {
                    return false; //max 10 numbers
                }

                var newWidget = list.attr('data-prototype');
                newWidget = newWidget.replace(/__name__/g, counter);

                counter++;

                list.data(' widget-counter', counter);

                var newElem = $(list.attr('data-widget-tags')).html(newWidget);
                newElem.appendTo(list);

                self.unlockSubmitButton();
                self.bindRemoveButton();
            });
        };

        this.unlockSubmitButton = function () {
            if ($('#number-fields-list li').length) {
                $(this.submitButtonSelector).removeAttr('disabled');
            } else {
                $(this.submitButtonSelector).removeAttr('disabled').attr('disabled', 'disabled');
            }
        };

        this.bindRemoveButton = function () {
            var self = this;

            $(this.removeButtonSelector).on('click', function () {
                $(this).closest('li').remove();
                self.unlockSubmitButton();
            });
        }
    }

    var controller = new PageController();
    controller.init();
});